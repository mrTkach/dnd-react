Hello Tech Team of AddComm!

For performing current task I was using React with combination of Typescript.
Just to present you functionality of current application – I mocked data of elements and data of boards (header, body, footer) and stored them in useState() which are placed in custom hook useLocalState().
In case current app will grow with real API call – separate hook/component with side effect can be easily implemented as well as one of state manager, where server data response can be stored.    

Across the components I left few comments which describes some of the actions in the functions.

I was following Figma design to implement UI part as needed.
Requirements of task duplicated below.

To have project running locally - clone the code from the repository, install packages/modules by running _npm install_ (or _yarn install_) and start the project by running _npm start_ (or _yarn start_).
Current project has been deployed to Gitlab Pages and is available at https://mrtkach.gitlab.io/dnd-react/, so there is no need to install it locally.

It was interesting task. I'm completely open for critics and suggestions regarding code improvements.

-------------------------


Tech Task:
_The assignment requires the development of the attached design proposal, with careful attention given to the following considerations:
The canvas consists of three distinct areas: Header, Body, and Footer. Users can drag elements from the sidebar and drop them into these areas.
The Body area accepts any type of element, while the Header area exclusively accepts Image elements, and the Footer area exclusively accepts Text elements.
Elements within an area can be rearranged.
Elements can be reordered between the Header and Body areas but moving a Text element from the Body to the Footer or moving a Text from the Footer to the Body is not permitted.
There is no limit to the number of elements that areas can hold._

Figma Design Proposal [Link](https://www.figma.com/file/kOCOpcJnpdSIG23AJJ3rr1/Front-Developer-%20Test?type=design&node-id=0:1&t=sKMWNeJy8kpM7Nxi-1).