import React, { useState, DragEvent } from "react";
import { Board, Item } from "./types";
import MainBoard from "./components/MainBoard";
import Sidebar from "./components/Sidebar";
import "./App.css";
import { useLocalState } from "./hooks/useLocalState";

const App = () => {
  const [currentBoard, setCurrentBoard] = useState<Board>(Object);
  const [currentItem, setCurrentItem] = useState<Item>(Object);
  const [hoveredBlockTypes, setHoveredBlockTypes] = useState<string>("");

  const { boards, setBoards } = useLocalState();

  const dragOverBoardHandler = (e: DragEvent<HTMLDivElement>): void => {
    e.preventDefault();
    const target = e.target as HTMLDivElement;
    const setOfDataset = target.dataset.set;
    setOfDataset && setHoveredBlockTypes(setOfDataset);
    if (!hoveredBlockTypes?.includes(currentItem.type)) {
      e.dataTransfer.dropEffect = "move";
    }
  };

  const dragLeaveBoardHandler = (e: DragEvent<HTMLDivElement>) => {
    e.preventDefault();
    setHoveredBlockTypes("");
  };

  const dropBoardHandler = (e: DragEvent<HTMLDivElement>, board: Board) => {
    if (!board.acceptedTypes?.includes(currentItem.type)) return;
    e.stopPropagation();
    board.items.push(currentItem);
    // we are getting index of selected item which we "handle in hand";
    // we need that index to delete it from array of currentBoard
    const currentIndex = currentBoard.items?.indexOf(currentItem);
    // deleting 1 item with index of currentIndex
    currentBoard.items.splice(currentIndex, 1);
    setBoards(
      boards.map((b: Board): Board => {
        // replacing existing boards to updated boards
        if (b.id === board.id) {
          return board;
        }
        if (b.id === currentBoard.id) {
          return currentBoard;
        }
        return b;
      }),
    );
  };

  return (
    <div className="app">
      <MainBoard
        boards={boards}
        setBoards={setBoards}
        currentItem={currentItem}
        setCurrentItem={setCurrentItem}
        currentBoard={currentBoard}
        setCurrentBoard={setCurrentBoard}
        dragOverBoardHandler={dragOverBoardHandler}
        dragLeaveBoardHandler={dragLeaveBoardHandler}
        dropBoardHandler={dropBoardHandler}
      />
      <Sidebar
        setBoards={setBoards}
        currentItem={currentItem}
        setCurrentItem={setCurrentItem}
        currentBoard={currentBoard}
        setCurrentBoard={setCurrentBoard}
        dragOverBoardHandler={dragOverBoardHandler}
        dragLeaveBoardHandler={dragLeaveBoardHandler}
        dropBoardHandler={dropBoardHandler}
      />
    </div>
  );
};

export default App;
