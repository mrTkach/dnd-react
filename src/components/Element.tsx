import React, { DragEvent } from "react";
import { Board, ElementProps, Item } from "../types";
import { ImageIcon, TableIcon, TextIcon } from "../assets/icons";
import styles from "../styles/Element.module.css";

const Element = ({
  item,
  board,
  boards,
  setBoards,
  currentBoard,
  setCurrentBoard,
  currentItem,
  setCurrentItem,
}: ElementProps) => {
  const icons = {
    table: <TableIcon />,
    text: <TextIcon />,
    image: <ImageIcon />,
  };

  const dragOverHandler = (e: DragEvent<HTMLDivElement>) => {
    e.preventDefault();
    const target = e.target as HTMLDivElement;
    if (target.classList.contains("element")) {
      target.style.boxShadow = "0 4px 3px grey";
    }
  };
  const dragLeaveHandler = (e: DragEvent<HTMLDivElement>) => {
    const target = e.target as HTMLDivElement;
    target.style.boxShadow = "none";
  };
  const dragStartHandler = (board: Board, item: Item) => {
    setCurrentBoard(board);
    setCurrentItem(item);
  };
  const dragEndHandler = (e: DragEvent<HTMLDivElement>) => {
    const target = e.target as HTMLDivElement;
    target.style.boxShadow = "none";
  };
  const dropHandler = (
    e: DragEvent<HTMLDivElement>,
    board: Board,
    item: Item,
  ) => {
    e.stopPropagation();
    if (!board.acceptedTypes?.includes(currentItem.type)) return;
    // we are getting index of selected item which we "handle in hand";
    // we need that index to delete it from array of currentBoard
    const currentIndex = currentBoard.items?.indexOf(currentItem);
    // deleting 1 item with index of currentIndex
    currentBoard.items.splice(currentIndex, 1);
    // getting index of element, over which we hold previously kept item
    // after which we want to place new kept item
    const dropIndex = board.items?.indexOf(item);
    // items of board, where we keep over new task
    // running splice() with new parameters for "new" board items
    board.items.splice(dropIndex + 1, 0, currentItem);
    // dropIndex + 1 - because we place after, deleting 0 items, currentItem - item we want to place
    const target = e.target as HTMLDivElement;
    target.style.boxShadow = "none";
    setBoards(
      boards.map((b: Board): Board => {
        // replacing existing boards to updated boards
        if (b.id === board.id) {
          return board;
        }
        if (b.id === currentBoard.id) {
          return currentBoard;
        }
        return b;
      }),
    );
  };

  return (
    <div
      key={item.id}
      draggable={true}
      className={`${styles.singleElement} element`}
      onDragOver={(e: DragEvent<HTMLDivElement>) => dragOverHandler(e)}
      onDragLeave={(e: DragEvent<HTMLDivElement>) => dragLeaveHandler(e)}
      onDragStart={() => dragStartHandler(board, item)}
      onDragEnd={(e: DragEvent<HTMLDivElement>) => dragEndHandler(e)}
      onDrop={(e: DragEvent<HTMLDivElement>) => dropHandler(e, board, item)}
    >
      {icons[item.type]}
      <p>{item.type}</p>
    </div>
  );
};

export default Element;
