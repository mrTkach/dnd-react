import React from "react";
import { Board, MainBoardProps } from "../types";
import Element from "./Element";
import styles from "../styles/MainBoard.module.css";

const MainBoard = ({
  boards,
  setBoards,
  currentItem,
  setCurrentItem,
  currentBoard,
  setCurrentBoard,
  dragOverBoardHandler,
  dragLeaveBoardHandler,
  dropBoardHandler,
}: MainBoardProps) => {
  return (
    <div className={styles.mainBoard}>
      {boards.map((board: Board) => (
        <div
          className={styles.boardWrapper}
          key={board.id}
          onDragOver={(e) => dragOverBoardHandler(e)}
          onDragLeave={(e) => dragLeaveBoardHandler(e)}
          onDrop={(e) => dropBoardHandler(e, board)}
        >
          <div className={styles.boardTitle}>{board.title}</div>
          <div
            key={board.id}
            data-set={board.acceptedTypes}
            className={styles.boardSlot}
            style={
              board.items.length === 0
                ? { border: "1px dashed #3a6b88" }
                : { border: "none" }
            }
          >
            {board.items.length === 0 && (
              <p
                className={styles.boardPlaceholder}
                data-set={board.acceptedTypes}
              >
                Drag and drop an element within this area.
              </p>
            )}
            <>
              {board.items.map((item) => (
                <Element
                  key={item.id}
                  item={item}
                  board={board}
                  boards={boards}
                  setBoards={setBoards}
                  currentItem={currentItem}
                  setCurrentItem={setCurrentItem}
                  currentBoard={currentBoard}
                  setCurrentBoard={setCurrentBoard}
                />
              ))}
            </>
          </div>
        </div>
      ))}
    </div>
  );
};

export default MainBoard;
