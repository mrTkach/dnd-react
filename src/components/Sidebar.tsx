import React, { DragEvent } from "react";
import { Item, SidebarProps } from "../types";
import { useLocalState } from "../hooks/useLocalState";
import Element from "./Element";
import styles from "../styles/Sidebar.module.css";

const Sidebar = ({
  currentItem,
  setCurrentItem,
  currentBoard,
  setCurrentBoard,
  dragOverBoardHandler,
  dragLeaveBoardHandler,
  dropBoardHandler,
}: SidebarProps) => {
  const { sidebarBoard, setSidebarBoard } = useLocalState();

  return (
    <div
      className={styles.sidebarBoard}
      onDragOver={(e: DragEvent<HTMLDivElement>) => dragOverBoardHandler(e)}
      onDragLeave={(e: DragEvent<HTMLDivElement>) => dragLeaveBoardHandler(e)}
      onDrop={(e: DragEvent<HTMLDivElement>) =>
        dropBoardHandler(e, sidebarBoard[0])
      }
    >
      <div className={styles.boardTitle}>{sidebarBoard[0].title}</div>
      <div className={styles.itemsWrapper}>
        {sidebarBoard[0].items.map((item: Item) => (
          <Element
            key={item.id}
            item={item}
            board={sidebarBoard[0]}
            boards={sidebarBoard}
            setBoards={setSidebarBoard}
            currentItem={currentItem}
            setCurrentItem={setCurrentItem}
            currentBoard={currentBoard}
            setCurrentBoard={setCurrentBoard}
          />
        ))}
      </div>
    </div>
  );
};

export default Sidebar;
