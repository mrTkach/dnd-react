import { useState } from "react";
import { Board } from "../types";

export const useLocalState = () => {
  const [boards, setBoards] = useState<Board[]>([
    {
      id: 1,
      title: "Header",
      acceptedTypes: ["image"],
      items: [],
    },
    {
      id: 2,
      title: "Body",
      acceptedTypes: ["table", "text", "image"],
      items: [],
    },
    {
      id: 3,
      title: "Footer",
      acceptedTypes: ["text"],
      items: [],
    },
  ]);

  const [sidebarBoard, setSidebarBoard] = useState<Board[]>([
    {
      title: "Elements",
      items: [
        { id: 1, type: "text" },
        { id: 2, type: "image" },
        { id: 3, type: "table" },
        { id: 4, type: "text" },
        { id: 5, type: "image" },
        { id: 6, type: "table" },
        { id: 7, type: "text" },
        { id: 8, type: "image" },
        { id: 9, type: "table" },
        { id: 10, type: "text" },
        { id: 11, type: "image" },
        { id: 12, type: "image" },
        { id: 13, type: "table" },
        { id: 14, type: "text" },
        { id: 15, type: "image" },
        { id: 16, type: "table" },
        { id: 17, type: "text" },
        { id: 18, type: "image" },
        { id: 19, type: "table" },
        { id: 20, type: "table" },
        { id: 21, type: "image" },
        { id: 22, type: "image" },
        { id: 23, type: "table" },
        { id: 24, type: "text" },
        { id: 25, type: "image" },
        { id: 26, type: "table" },
        { id: 27, type: "text" },
        { id: 28, type: "image" },
        { id: 29, type: "table" },
      ],
    },
  ]);
  return { boards, setBoards, sidebarBoard, setSidebarBoard };
};
