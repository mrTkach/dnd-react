import { DragEvent } from "react";

export type Type = "table" | "text" | "image";
export type Title = "Header" | "Body" | "Footer" | "Elements";

export type Item = {
  id: number;
  type: Type;
};

export type Board = {
  id?: number;
  title: Title;
  acceptedTypes?: Type[];
  items: Item[];
};

export type MainBoardProps = {
  boards: Board[];
  setBoards: (el: Board[]) => void;
  currentItem: Item;
  setCurrentItem: (el: Item) => void;
  currentBoard: Board;
  setCurrentBoard: (el: Board) => void;
  dragOverBoardHandler: (e: DragEvent<HTMLDivElement>) => void;
  dragLeaveBoardHandler: (e: DragEvent<HTMLDivElement>) => void;
  dropBoardHandler: (e: DragEvent<HTMLDivElement>, board: Board) => void;
};

export type ElementProps = {
  item: Item;
  board: Board;
  boards: Board[];
  setBoards: (el: Board[]) => void;
  currentBoard: Board;
  setCurrentBoard: (el: Board) => void;
  currentItem: Item;
  setCurrentItem: (el: Item) => void;
};

export type SidebarProps = Omit<MainBoardProps, "boards">;

export type IconProps = { width?: string; height?: string; fill?: string };
