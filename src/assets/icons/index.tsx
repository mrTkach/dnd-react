import ImageIcon from "./ImageIcon";
import TableIcon from "./TableIcon";
import TextIcon from "./TextIcon";

export { ImageIcon, TableIcon, TextIcon };
